from read_data import data
from isa import ISA
from class_1_weight import class_i_weight
from xfoil import airfoil, get_max_cl
import os
import json
from copy import deepcopy
import matplotlib.pyplot as plt

isa = ISA()
airbas = data.all_aircraft[-1]

m_mto = airbas['m_mto']
S = airbas['S']
o_ws = m_mto * 9.81 / S
v_cruise = airbas['v_cruise']

start_cruise_ws = o_ws * (0.99 * 0.995 * 0.995)
end_cruise_ws = start_cruise_ws * class_i_weight()['mff']

h_cruise = airbas['h_ceiling']  # [m]
q_cruise = 1/2 * isa.get(h_cruise)['\rho'] * v_cruise ** 2

# This is the design lift coefficient for which we will look for the aircraft
c_L_des = 1.1/ (2*q_cruise) * (start_cruise_ws + end_cruise_ws)

c_mac = 2.67  # [m]
nu = 8.02*10**-6  # kinematic viscosity [m^2/s]

reynolds = v_cruise * c_mac / nu

# HERE IS WHERE THE CALCULATIONS FOR ALL THE AIRCRAFT START
# HERE IS WHERE THE CALCULATIONS FOR ALL THE AIRCRAFT START

try:
    f = open(r"results/airfoil_result.list", 'r')
    airfoils_str = f.read()
    f.close()

    airfoils = json.loads(airfoils_str)
except FileNotFoundError:
    print("Finding new airfoils")
    all_airfoils = [a for a in os.listdir('airfoils')]

    airfoils = []
    for a in all_airfoils:
        air = airfoil(a, reynolds, C_L=c_L_des)
        if air is not False and round(air['CL'], 2) == round(c_L_des, 2):
            airfoils.append(air)
    f = open('results/airfoil_result.list', 'w')
    f.write(json.dumps(airfoils))
    f.close()

lowest_cd = sorted(airfoils, key=lambda a: a['CD'])
lowest_cd = deepcopy([low for low in lowest_cd if low['CD'] and low['CL'] is not ''][:10])
# for el in lowest_cd:
#     print(el['naca'])

to_remove = []
for low in lowest_cd:
    res = get_max_cl(low['naca'], reynolds)
    if res is not None:
        low['Cl_max'], low['alpha_list'], low["c_l_list"], low["c_d_list"] = res
    else:
        print("Fuck", low['naca'])
        continue
    low["LD_cruise"] = low['CL'] / low['CD']

    plt.rcParams.update({'mathtext.default': 'regular'})

    if low['naca'] + ".pdf" not in os.listdir('results'):
        fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True)

        ax1.plot(low["c_d_list"], low["c_l_list"])
        ax1.set_xlabel(r'$C_d$')
        ax1.set_ylabel(r'$C_l$')
        ax1.set_title(r"$C_l$ vs $C_d$")

        ax2.set_title("Lift curve")
        ax2.set_xlabel(r"$\alpha$ [deg]")
        ax2.set_ylabel(r"$C_l$")
        ax2.plot(low["alpha_list"], low["c_l_list"])

        fig.suptitle(low['naca'])
        plt.show()
        plt.savefig("results/" + low['naca'] + ".pdf", format="pdf")

        plt.clf()
        plt.cla()

# doing it while the list is iterating creates issues because the for loop then skips
# over the element that was about the be iterated over.
for el in to_remove:
    print('removed')
    lowest_cd.pop(lowest_cd.index(el))

highest_cl_max = sorted(lowest_cd, key=lambda a: a['Cl_max'])
# [print(a['naca']) for a in highest_cl_max]
final_airfoil = highest_cl_max[-2]

if __name__ == "__main__":
    fig = plt.figure()
    ax = fig.gca(projection="3d")
    ax.plot(final_airfoil["alpha_list"], final_airfoil['c_l_list'], final_airfoil['c_d_list'])
    ax.legend()

    plt.show()
