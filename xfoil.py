import os
import time
from copy import deepcopy
from subprocess import Popen, PIPE


naca_number = 2412
reynolds = "615e5"
C_L = 0.37


def airfoil(filename, reynolds_number, C_L=None):
    if C_L is not None:
        text = """
        LOAD airfoils/{}

        PPAR
        N 200
        
        
        
        OPER
        Visc {}
        ITER 20
        M 0.68
        Cl {}

        

        """.format(filename, reynolds_number, C_L)

    with open('commands', 'w') as f:
        f.write(text)
        f.close()

    pi = Popen('type commands', shell=True, stdout=PIPE)
    p = Popen(['xfoil'], stdout=PIPE, stdin=pi.stdout, stderr=PIPE, shell=True)
    data = str(p.communicate()[0]).split("\\n")

    if data[-1].find("exception") is not -1:
        return None

    useful = deepcopy(data[-11:-3])
    if useful[-1][0:5] == " Type":  # There was a convergence error
        useful.pop(-1)
        useful.pop(-2)

    datastr = ""
    for line in useful:
        datastr += line

    values = {'naca': filename.replace('.dat', '')}

    for value in ['CD', 'CDf', 'CDp', 'Cm', 'CL', 'a']:
        loc = datastr.find(value + " = ")
        val = datastr[loc+5: loc+12]
        try:
            values[value] = float(val.strip())
        except:
            return False
    return values


def get_max_cl(filename, reynolds_number):
    if filename+".txt" not in os.listdir():
        text = """
        LOAD airfoils\{0}.dat
        PPAR
        N 200
        
        
        
        OPER
        Visc {1}
        PACC 
        {0}.txt
        
        ITER 100
        Aseq -2 30 0.1
        
        QUIT
    
        """.format(filename, reynolds_number)
        with open('commands', 'w') as f:
            f.write(text)
            f.close()
        from subprocess import Popen, PIPE
        pi = Popen('type commands', shell=True, stdout=PIPE)
        p = Popen(['xfoil'], stdout=PIPE, stdin=pi.stdout, stderr=PIPE, shell=True)
        try:
            p.communicate()[0]
        except KeyboardInterrupt:
            print("SKIPPED")
            time.sleep(1)

    # Let's read the text file now:
    f = open(filename+".txt", 'r')
    data = f.readlines()[12:]
    assert data != []
    c_l_max = max(float(a[11:17]) for a in data)

    alpha = [float(a[2:7]) for a in data]
    c_l_list = [float(a[10:16]) for a in data]
    c_d_list = [float(a[19:26]) for a in data]

    f.close()

    return c_l_max, alpha, c_l_list, c_d_list
