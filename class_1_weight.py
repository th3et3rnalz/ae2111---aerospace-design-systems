def class_i_weight(w_oe=None):
    from read_data import data
    # from M_OE_vs_M_MTO import function_values
    from math import exp

    # All the formula used on this page come from the second lecture of the AE1222 - II
    # Aerospace Design & Systems Engineering Elements.

    # To make a class 1 weight estimation, we use a formula that has certain inputs.
    # Let's begin by getting out aircraft:

    ac = data.all_aircraft[-1]  # It'll always be the last in the list because it's
    # added separately

    # Some variables we need, constants are from the lecture slides

    a, b = data.graph('m_mto', 'm_oe', fit=1)[1]
    # print(a,b)
    g = 9.81
    l_d_cruise = 15.547  # Lift to drag cruise, from slides
    l_d_loiter = 13  # Lift to drag loiter, from slides
    # From ADSEE Slides we have c_j_cruise = 1.9827*10**-5
    # Engine PW305B from pratt&whitney (Canada)
    # engine mass = 450kg * 2 engines
    #
    c_j_cruise = 1.929*10**-5  # [kg/Ns]Specific mass consumption of a jet engine.From slides.
    c_j_loiter = c_j_cruise  # [kg/Ns]Specific mass consumption of a jet engine.From slides.
    t_loiter = 30 * 60  # loiter time [s]

    m_payload = ac['m_payload']  # [kg]
    v_cruise = ac['v_cruise']  # [m/s]
    r_ferry = (ac['r_ferry'] + 463) * 1000  # [m]
    r_mission = (ac['r_mission'] + 463) * 1000  # [m]
    r_harmonic = (ac['r_harmonic'] + 463) * 1000  # [m]

    # Now that we have all these constants, the only one we need to calculated is the mass
    # fuel fraction. This is dependent on the critical mission profile.
    # We will be repeating the calculations three tines, once for each of the range required.

    # The mission profile that I am calculating for involves a take-off, cruise, loiter and
    # then landing. This means that the mass fractions are fixed except for the loiter and cruise
    # portions, the remaining mass fraction is:
    mf_rest = 0.99 * 0.995 * 0.995 * 0.98

    # To calculate mass fraction for cruise:
    def mf_cruise(r_range):
        # I subdivide equation to simplify checking
        b_exponent = (v_cruise / (g * c_j_cruise))*l_d_cruise
        mf_c = 1/exp(r_range / b_exponent)
        return mf_c

    # The loiter mf fraction depends on time, and not the mission profile.
    bottom_exponent = (1/(g*c_j_loiter)) * l_d_loiter
    mf_loiter = 1 / exp(t_loiter / bottom_exponent)

    mff_ferry = mf_cruise(r_ferry)*mf_rest * mf_loiter
    mff_mission = mf_cruise(r_mission)*mf_rest * mf_loiter
    mff_harmonic = mf_cruise(r_harmonic)*mf_rest * mf_loiter

    # We now have all the variables required for calculating the take_off_weight for each
    # mission profile
    if w_oe is None:
        mto_ferry = b / (1 - a - (1-mff_ferry))
        mto_mission = (b + m_payload) / (1 - a - (1-mff_mission))
        mto_harmonic = (b + m_payload) / (1 - a - (1-mff_mission))
    else:
        mto_ferry = w_oe / mff_ferry
        mto_mission = (w_oe + m_payload) / mff_mission
        mto_harmonic = (w_oe + m_payload) / mff_harmonic

    # CRITICAL MISSION PROFILE
    mto_s = [mto_ferry, mto_harmonic, mto_mission]

    mto_critical = max(mto_s)
    index = mto_s.index(mto_critical)
    mff_critical = [mff_ferry, mff_harmonic, mff_mission][index]

    m_oe = a * mto_critical + b
    m_fuel = (1 - mff_critical) * mto_critical

    # Now these values can easily be imported by another file and plotted.
    class_1 = {'m_mto': mto_critical, 'm_oe': m_oe, 'm_fuel': m_fuel, 'mff': max(mff_mission, mff_ferry, mff_harmonic)}
    return class_1
