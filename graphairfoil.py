import matplotlib.pyplot as plt


def make_figure(airfoil):
    plt.rcParams.update({'font.size': 16})
    # f = open(r"airfoils/{}.dat".format(airfoil['naca']), 'r')
    data = open('our_airfoil.txt', 'r').readlines()[2:]
    # x, y = [], []
    # data = f.readlines()[1:-2]
    # for line in data:
    #     x_i, y_i = line.split(" ")
    #     x.append(float(x_i.strip()))
    #     y.append(float(y_i.strip()))
    # f.close()
    x = [float(line[:17]) for line in data]
    y = [float(line[17:31]) for line in data]

    plt.clf()
    plt.cla()

    plt.xlim(-0.1, 1.1)
    plt.ylim(-0.2, 0.2)
    plt.gca().set_aspect('equal', adjustable='box')
    plt.plot(x, y)
    # plt.title(airfoil['naca'])
    plt.xlabel("x/c")
    plt.ylabel("t/c")
    plt.tight_layout()
    plt.savefig("results/final/" + airfoil['naca']+"_airfoil.pdf", format="pdf")
    # plt.show()
    plt.clf()
    plt.cla()

    plt.rcParams.update({'mathtext.default': 'regular'})
    plt.plot(airfoil['alpha_list'], airfoil['c_l_list'])
    plt.xlabel(r'$\alpha [deg]$')
    plt.ylabel(r'$C_l [-]$')
    # plt.title(airfoil['naca'])
    plt.grid()
    plt.tight_layout()
    plt.savefig('results/final/'+ airfoil['naca'] + "_lift_drag_curve.pdf", format="pdf")
    # plt.show()
    plt.clf()
    plt.cla()

    plt.plot(airfoil['c_d_list'], airfoil['c_l_list'] )
    plt.rcParams.update({'mathtext.default': 'regular'})
    plt.xlabel(r'$C_d [-]$')
    plt.ylabel(r'$C_l [-]$')
    plt.grid()
    plt.tight_layout()
    # plt.title(airfoil['naca'])
    plt.savefig('results/final/'+ airfoil['naca'] + "_drag_polar.pdf", format="pdf")
    # plt.show()
    plt.clf()
    plt.cla()

    return True
