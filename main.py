"""
This file will coordinate all the others and coordinate the flow of information
"""
from math import sqrt
import time
start = time.time()
from read_data import data  # This will read all the data from the excel sheet
airbas = data.all_aircraft[-1]


# CLASS 1 WEIGHT ESTIMATION
from class_1_weight import class_i_weight
airbas['m_mto'] = class_i_weight()['m_mto']
airbas['m_oe'] = class_i_weight()['m_oe']
airbas['m_fuel'] = airbas['m_mto'] - airbas['m_oe']
airbas['AR'] = 8.32

# statistical estimate of the stall speed:
f, z = data.graph('v_stall', 'd_landing', fit=2)
# some fumbling around because plofyfit only does polynomials of n-th degrees, not degree 0.5 as we want.
v_stall = (- z[1] + sqrt(z[1]**2 - 4 * z[0] * (z[2] - airbas['d_landing']))) / (2 * z[0])
airbas['v_stall'] = v_stall
airbas['v_landing'] = v_stall * 1.1

# T/W vs W/S ESTIMATING W AND S
from TW_vs_WS import design_point
airbas['S'] = airbas['m_mto'] * 9.81 / design_point['W/S']
airbas['T'] = design_point['T/W'] * airbas['m_mto'] * 9.81

# PLANFORM DESIGN
from planform import planform
airbas['planform'] = planform()

# AIRFOIL DESIGN  AIRFOIL DESIGN 
from airfoil import final_airfoil
from graphairfoil import make_figure
print(final_airfoil['naca'])
make_figure(final_airfoil)


import aircraft_iteration_tools


from DragEstimation import aerodynamic
airbas['aero'] = aerodynamic

if __name__ == "__main__":
    airbas.print_all()

print(f"Computation time {time.time() - start}")


# VOOR BAS  -   VOOR BAS  -   VOOR BAS  -   VOOR BAS  -   VOOR BAS
# Zou jij voor mij dan in de drag estimation de volgende dingen kunnen doen?
# - Mcrit berekenen
# - Checken of alle equations in de tekst gerefered en gecite zijn - checl
# - De values updaten met de original values
