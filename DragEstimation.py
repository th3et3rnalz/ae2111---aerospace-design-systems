"""
Drag Estimation for Work Package 3 of Systems Design
"""
from read_data import data
from math import radians, sqrt, log, cos, pi, tan, atan
airbas = data.all_aircraft[-1]
# from decimal import Decimal
# #Variables

Vcr = airbas['v_cruise']
Vland = 42.727034905941686
Mcr = 0.68
Mland = 0.1269782828
rhocr = 0.2873681087078372
Srefwing = airbas['S']
Srefhtail = airbas['tails']['h_tail']['S_h']
Srefvtail = airbas['tails']['v_tail']['S_v']
Cr = airbas['planform']['c_r']
Ct = airbas['planform']['c_t']
Cmacwing = airbas['planform']['mac']
Cmachtail = airbas['tails']['h_tail']['mac_h']
Cmacvtail = airbas['tails']['v_tail']['mac_v']
taper = airbas['planform']['lambda']
ksurface = 0.052 * 10**(-5)  # surface roughness factor
mu = 1.43226 * 10**(-5)
Lfus = 19.
Dfus = 2.
Leng = 2.07
Deng = 0.7874
Dbase = 0.4
xcm6 = 0.349   # location of maximum thickness per chord (NACA 644221)
xcm0ht = 0.3   # location of maximum thickness per chord (NACA 0012)
xcm0vt = 0.3   # location of maximum thickness per chord (NACA 0012)
sweepm6 = atan(tan(airbas['planform']['sweep_0c']) -
               xcm6*(2*Cr)/(airbas['planform']['b'])*(1-airbas['planform']['lambda']))
# sweep at maximum thickness main wing
sweepm0ht = atan(tan(airbas['tails']['h_tail']['sweep_0c_h']) -
                 xcm6*(2*Cr)/(airbas['tails']['h_tail']['b_h'])*(1 -
                 airbas['tails']['h_tail']['c_t_h']/airbas['tails']['h_tail']['c_r_h']))
# sweep at maximum thickness horizontal tail
sweepm0vt = atan(tan(airbas['tails']['v_tail']['sweep_0c_v']) -
                 xcm6*(2*Cr)/(airbas['tails']['v_tail']['b_v'])*(1 -
                 airbas['tails']['v_tail']['c_t_v']/airbas['tails']['v_tail']['c_r_v']))
# sweep at maximum thickness vertical tail

tc6 = 0.21
tc0ht = 0.1
tc0vt = 0.12
Lnc = 3.
Ltc = 8.
Lcyl = 8.
leakagefactor = 1.02
Fflap = 0.0074
Sflapref = 0.52
cfc = 0.3
u = radians(11.)
delta = 40


lengths = [Lfus, Cmacwing, Leng, Cmachtail, Cmacvtail]
diameters = [Dfus, 0, Deng, 0, 0]
surfaces = [0, Srefwing, 0, Srefhtail, Srefvtail]
xcms = [0, xcm6, 0, xcm0ht, xcm0vt]
sweepms = [0, sweepm6, 0, sweepm0ht, sweepm0vt]
tcs = [0, tc6, 0, tc0ht, tc0vt]
speeds = [Vcr, Vland]
machs = [Mcr, Mland]
Re = []
Cf = []
FF = []
Swet = []
Fusdrag = []

#Interference factors

IFfus = 1.
IFwing = 1.4
IFeng = 1.3
IFhtail = 1.04
IFvtail = 1.04

IF = [IFfus, IFwing, IFeng, IFhtail, IFvtail, IFfus, IFwing, IFeng, IFhtail, IFvtail]

#Calculate parameters


k = 0
while k < 2:
    i = 0
    while i < 5:

        #Re
        
        Reynolds = min((rhocr * speeds[k] * lengths[i])/mu,38.21*(lengths[i]/ksurface)**1.053)
        Re.append(Reynolds)

        #Cf

        Cflam = 1.328 / sqrt(float(Re[i]))
        Cfturb = 0.455 / ( ((log (float(Re[i]),10))**(2.58)) * ((1 + 0.144 * (machs[k])**2)**(0.65)))
        if i == 1 or i == 3 or i == 4:
            Cftot = 0.5 * Cflam + 0.5 * Cfturb
        else:
            Cftot = 0.25 * Cflam + 0.75 * Cfturb
        Cf.append(Cftot)

        #FF

        if i == 1 or i == 3 or i == 4:
            FoFa = (1 + (0.6/xcms[i]) * tcs[i] + 100 * (tcs[i])**4)*(1.34*((machs[k])**0.18)*((cos(sweepms[i]))**0.28))
        else:
            f = lengths[i]/diameters[i]
            if i == 0:
                FoFa = 1 + 60./(f**3) + f/400.
            else:
                FoFa = 1 + 0.35/f
        FF.append(FoFa)

        #Swet
        
        if i == 1:
            Swetted = 1.07 * 2. * surfaces[i]
        if i == 2:
            Swetted = 2. * 2. * pi * diameters[i] * lengths[i]
        if i == 3 or i == 4:
            Swetted = 1.05 * 2. * surfaces[i]
        if i == 0:
            A = pi * diameters[i] / 4.
            B = 1. / (3. * Lnc * Lnc)
            C = 4. * Lnc * Lnc + (diameters[i])**2/4
            D = (diameters[i])**3/8.
            E = - diameters[i] + 4.*Lcyl
            F = 2. * sqrt(Ltc*Ltc + (diameters[i])**2/4.)
            Swetted = A * (B * (C**1.5 - D) + E + F) 

        Swet.append(Swetted)
        
        i = i + 1

    fuselage_drag = (3.83 * u**2.5)*pi*(Dfus**2/4) + (0.139 + 0.419*(machs[k]-0.161)**2)*pi*(Dbase**2/4)
    Fusdrag.append(fuselage_drag)
    k = k + 1

Sref = Srefwing 

j = 0
l = 5
sumdrag1 = 0
sumdrag2 = 0

while j < 5:
    sumdrag1 = sumdrag1 + Cf[j]*FF[j]*IF[j]*Swet[j]
    j = j + 1

sumdrag1 = sumdrag1 + Fusdrag[0]
while l < 10:
    sumdrag2 = sumdrag2 + Cf[l]*FF[l]*IF[l]*Swet[l]
    l = l + 1

sumdrag2 = sumdrag2 + Fusdrag[1]

Cd0cruise = sumdrag1 / Sref
Cd0land = sumdrag2 / Sref + Fflap * cfc * Sflapref * (delta - 10)


# print("The following data has been found, in the order of:CRUISE(fuselage, wing, engine, horizontal tail and "
#       "vertical tail), LANDING(fuselage, wing, engine, horizontal tail and vertical tail)")
# print()
# print("Reynolds numbers (Re):")
# print(Re)
# print()
# print("Total flat plate skin friction coefficients (Cf):")
# print(Cf)
# print()
# print("Form factors (FF):")
# print(FF)
# print()
# print("Wetted areas (Swet [m^2]):")
# print(Swet)
# print()
# print("The estimated zero-lift drag coefficient for cruise conditions is:", Cd0cruise)
# print()
# print("The estimated zero-lift drag coefficient for landing conditions is:", Cd0land)

aerodynamic = {'C_D_0_cruise': Cd0cruise, 'C_D_0_land': Cd0land}
