from math import *


class ISA:
    def __init__(self):
        self.p0 = 101325  # 108900
        self.T0 = 288.15  # 292.15
        self.h_base = 0  # -610

        self.temp = self.T0
        self.p = self.p0
        self.d = self.p0 / (287 * self.T0)

        self.data = []

    def calculate(self, a, h_ceiling, h0, h1, t_base, p_base):
        if h1 > h0:
            if h1 > h_ceiling:
                h1 = h_ceiling
            if a != 0:
                self.temp = t_base + a*(h1 - h0)
                self.p = p_base*pow(self.temp/t_base, -9.80665/(a*287))
            else:
                self.p = p_base*exp((-9.80665/(287*t_base))*(h1-h0))
                self.temp = t_base
            self.d = self.p/(287*self.temp)  # Density
            self.data = [self.temp, self.p, h_ceiling]

    def get(self, altitude):  # This is the function you'll want to call
        self.calculate(-0.0065, 11000, self.h_base, altitude, self.T0, self.p0)
        self.calculate(0, 20000, self.data[2], altitude, self.data[0], self.data[1])
        self.calculate(0.001, 32000, self.data[2], altitude, self.data[0], self.data[1])
        self.calculate(0.0028, 47000, self.data[2], altitude, self.data[0], self.data[1])
        self.calculate(0, 51000, self.data[2], altitude, self.data[0], self.data[1])
        self.calculate(-0.0028, 84852, self.data[2], altitude, self.data[0], self.data[1])
        self.calculate(0, 100000, self.data[2], altitude, self.data[0], self.data[1])

        data = {'T': self.temp,
                '\rho': self.d,
                'p': self.p
                }
        return data
