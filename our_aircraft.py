from aircraft import Aircraft
from isa import ISA  # this is to generate the cruise speed dynamically
from math import *


"""
In this file we enter the requirements.
When the Data class is called (this happens when all the other aircraft are being entered into the system) then this data will also be entered.

We can ammend this when we get more information, if necessary
"""

# THIS IS WHERE YOU ENTER STUFF
m_payload = 1010  # kg
M_cruise = 0.68  # at 4100ft
h_cruise = 41000 * 0.3048 # m
d_landing = 620  # m
d_take_off = 1250  # m
r_mission = 4482  # km
r_harmonic = 6100  # km
r_ferry = 7000  # km

# THIS IS WHERE THE DATA FOR THE AIRCRAFT CLASS IS PUT TOGETHER

isa = ISA()
temp_cruise = isa.get(h_cruise)['T']
a_cruise = sqrt(1.4 * 287 * temp_cruise)
v_cruise = a_cruise * M_cruise

data_dict = {'name': "Airbas A8",  # Aircraft name
            'year': '2019',  # First production year
            'mach_cruise': M_cruise,  # Cruise Mach number
            'v_cruise': v_cruise,  # Cruise speed [m/s]
            'v_stall': None,  # Stall speed
            'd_take_off': d_take_off,  # Take off distance [m], all engines operating
            'd_landing': d_landing,  # Landing distance [m]
            'r_mission': r_mission,  # Mission range [km]
            'r_harmonic': r_harmonic,  # Harmonic range [km]
            'r_ferry':  r_ferry,  # Ferry range [km]
            'h_ceiling': h_cruise,  # Ceiling altitude [m]
            'm_mto': None,  # Max take-off weight [kg]
            'm_ml': None,  # Max landing weight [kg]
            'm_oe': None,  # Operating empty weight [kg]
            'm_payload': m_payload,  # Payload weight at max fuel [kg, should be]
            'l_cabin': None,  # Cabing length [m]
            'w_cabin': None,  # Cabin width [m]
            'h_cabin': None,  # Cabin height [m]
            'v_cabin': None,  # Cabin volume [m]
            'max_seating': None,  # Max seating
            'typical_seating': None,  # Typical seating
            'length': None,  # Aircraft length
            'wingspan': None,  # Wingspan
            'height': None,  # Height
            'AR': None, # Aspect ratio
            'c_root': None,  # Wing chord root
            'c_tip': None,  # Wing chord tip
            'c_mean': None,  # Wing chord mean
            'ws_max': None,  # Max wing loading
            'ts_max': None,  # Max thrust loading
            'g_max': None,  # G limit
            'S': None,  # wing surface area
            'sweep': None,  # Wing sweep
            'sweep-remark': None,  # Remark about where sweep is measured
            'n': None,  # Number of engines
            'T': None,  # Total thrust
            }

our_business_jet = Aircraft(data_dict=data_dict)