
def cg_excursion(mass_dict):
        from read_data import data
        from math import pi
        import matplotlib.pyplot as plt
        airbas = data.all_aircraft[-1]
        
        #inputs.
        ##Wing Group
        wingMI = mass_dict['m_wing']/mass_dict['m_mto']  # 0.062  # wing weight divided by mto
        WCG = 0.4
        
        ##Fuselage Group
        empennageMI = mass_dict['m_empennage']/mass_dict['m_mto']  # empennage weight divided by mto
        empennageX = 0.95 * 19
        fuselageMI = 0.08
        fuselageX = 7.6
        fixedequipmentMI = mass_dict['m_equipment']/mass_dict['m_mto']  # fixed weight weight divided by mto
        fixedequipmentX = 7.6
        nacelleMI = mass_dict['m_nacelle']/mass_dict['m_mto']  # same for nacelle
        nacelleX = 13
        propulsionMI = mass_dict['m_powerplant']/mass_dict['m_mto']  # same for powerplant
        propulsionX = 13
        OEWCG = 0.25
        
        ##general
        CG_Payload = 7.9
        OEW = mass_dict['m_oe']
        Fuel = airbas['m_fuel']
        Payload = airbas['m_payload']
        MAC = airbas['planform']['mac']
        CR = airbas['planform']['c_r']
        angleLE = 26.2 * pi / 180
        YMAC = airbas['planform']['y_lemac']
        
        # ==================================================================================================
        #     CALCULATIONS  -   CALCULATIONS  -   CALCULATIONS  -   CALCULATIONS  -   CALCULATIONS
        # ==================================================================================================
        
        # X_FCG
        FG_sum_MI = empennageMI + fuselageMI + fixedequipmentMI + nacelleMI + propulsionMI
        FG_sum_MIX = empennageMI*empennageX + fuselageMI*fuselageX + fixedequipmentMI*fixedequipmentX + \
            nacelleMI*nacelleX + propulsionMI*propulsionX
        X_FCG = FG_sum_MIX / FG_sum_MI
        
        # X_LEMAC
        ratioWF = wingMI/FG_sum_MI
        X_LEMAC = X_FCG + MAC*(WCG*ratioWF-OEWCG*(1+ratioWF))
        
        # X_WING
        wingx = WCG*MAC
        X_WING = X_LEMAC + wingx
        
        
        # X_OEWCG
        FMIX = FG_sum_MIX
        WMIX = X_WING*wingMI
        FWMI = wingMI+FG_sum_MI
        X_OEWCG = (FMIX+WMIX)/FWMI
        
        # CG Excursion
        MTOW = OEW+Fuel+Payload
        CG_Fuel = X_WING
        FuelMI = Fuel/MTOW
        PayloadMI = Payload/MTOW
        OEWMI = OEW/MTOW
        
        CG_OEW = X_OEWCG
        CG_OEW_FUEL = (CG_Fuel*FuelMI+CG_OEW*OEWMI)/(FuelMI+OEWMI)
        CG_OEW_PAYLOAD = (CG_OEW*OEWMI+CG_Payload*PayloadMI)/(OEWMI+PayloadMI)
        CG_MTOW = (CG_Fuel*FuelMI+CG_OEW*OEWMI+CG_Payload*PayloadMI)/(FuelMI+OEWMI+PayloadMI)
        
        CG_Values = [CG_OEW, CG_OEW_FUEL, CG_OEW_PAYLOAD, CG_MTOW]
        cg_max = max(CG_Values)
        cg_min = min(CG_Values)
        CG_Excursion = cg_max - cg_min

        cg_result = {'X_oew_cg': X_OEWCG, 'W_wing': X_WING, 'X_lemac': X_LEMAC, 'X_fg': X_FCG, 'cg_oew': CG_OEW,
                     'cg_oew_fuel': CG_OEW_FUEL, 'cg_oew_payload': CG_OEW_PAYLOAD, 'cg_mtow': CG_MTOW,
                     'cg_excursion': CG_Excursion, 'cg_max': cg_max, 'cg_min': cg_min}
        
        x = CG_Values
        y = [airbas['m_oe'], airbas['m_oe'] + airbas['m_fuel'], airbas['m_oe'] + airbas['m_payload'], airbas['m_mto']]
        
        plt.plot(x[0:2], y[0:2], color='black')
        plt.plot([x[0], x[2]], [y[0], y[2]], color='black')
        plt.plot([x[2], x[3]], [y[2], y[3]], color='black')
        plt.plot([x[1], x[3]], [y[1], y[3]], color='black')
        plt.xlabel('X-pos [m]')
        plt.ylabel('Mass [kg]')
        plt.title('CG excursion')
        # plt.savefig('cg_excursion.pdf', format='pdf')
        # plt.show()
        
        return cg_result
