# class II weight estimation

from read_data import data
from class_2_weight import class_ii_weight
from CGExcursion import cg_excursion
from Empennage import empennage
from planform import planform
from class_1_weight import class_i_weight
from TW_vs_WS import design_point


airbas = data.all_aircraft[-1]
m_mto_class_i = airbas['m_mto']
class_ii_result = class_ii_weight(m_mto_class_i * 2.20462)
m_mto_class_ii = class_ii_result['m_mto']

iterations = 0
tails, cg_result = None, None
while abs(m_mto_class_i - m_mto_class_ii) > 0.01:  # just because we can
    iterations += 1

    # First we give the information to redo the cg_excursion and then the empennage
    cg_result = cg_excursion(class_ii_result)
    tails = empennage(cg_result)
    airbas['tails'] = tails

    # Let's add in the planform design, because why not:
    airbas['planform'] = planform()

    from DragEstimation import aerodynamic

    airbas['aero'] = aerodynamic

    class_ii_result = class_ii_weight(m_mto_class_i*2.20462, tail=tails, cg_excursion=cg_result, m_fuel=airbas['m_fuel'])
    m_mto_class_ii = class_ii_result['m_mto']

    class_i_result = class_i_weight(w_oe=class_ii_result['m_oe'])
    airbas['m_fuel'] = class_i_result['m_fuel']
    m_mto_class_i = class_i_result['m_mto']

    # Once we have the weight, let's redo our thrust and wing surface area:
    airbas['S'] = class_i_result['m_mto'] * 9.81 / design_point['W/S']
    airbas['T'] = design_point['T/W'] * class_i_result['m_mto'] * 9.81



# Cleaning up
airbas['m_mto'] = round(m_mto_class_ii, 3)
airbas['m_oe'] = round(m_mto_class_ii - airbas['m_fuel'] - airbas['m_payload'], 3)
airbas['class_II_mass'] = class_ii_weight(m_mto_class_ii * 2.20462)
airbas['tails'] = tails
airbas['cg_excursion'] = cg_result
print(f"After {iterations} iterations, new m_mto: {airbas['m_mto']}")
