from read_data import data
from math import pi
from airfoil import c_L_des
from class_1_weight import c_j_cruise
airbas = data.all_aircraft[-1]

CD_0 = 0.02522
drag = CD_0 + c_L_des ** 2 / (airbas['e']*airbas['AR']*pi)
print(drag)

SAR_val = airbas['v_cruise'] / (drag * c_j_cruise)

SAR = str(SAR_val * 10 ** -8) + "*10^8"
