class Aircraft:
    def __init__(self, data=None, data_dict=None):
        # This "data=None" and data_dict=None will enable us to enter data into the 
        # database in two ways: Through the excel sheet or manually.
        if data is not None:
            self.dict = {'name': data[1],  # Aircraft name
                        'year': data[3],  # First production year
                        'mach_cruise': data[8],  # Cruise Mach number
                        'v_cruise': data[7],  # Cruise speed [m/s]
                        'v_stall': data[32],  # Stall speed
                        'd_take_off': data[10],  # Take off distance [m]
                        'd_landing': data[11],  # Landing distance [m]
                        'r_mission': data[12],  # Mission range [km]
                        'r_harmonic': data[13],  # Harmonic range [km]
                        'r_ferry':  data[14],  # Ferry range [km]
                        'h_ceiling': data[15],  # Ceiling altitude [m]
                        'm_mto': data[16],  # Max take-off weight [kg]
                        'm_ml': data[17],  # Max landing weight [kg]
                        'm_oe': data[19],  # Operating empty weight [kg]
                        'm_payload': data[18],  # Payload weight at max fuel [kg]
                        'l_cabin': data[21],  # Cabing length [m]
                        'w_cabin': data[22],  # Cabin width [m]
                        'h_cabin': data[23],  # Cabin height
                        'v_cabin': data[24],  # Cabin volume [m]
                        'max_seating': data[25],  # Max seating
                        'typical_seating': data[26],  # Typical seating
                        'length': data[28],  # aircraft length
                        'wingspan': data[30],  # Wingspan
                        'height': data[31],  # Height
                        'AR': data[33], # Aspect ratio
                        'c_root': data[34],  # Wing chord root
                        'c_tip': data[35],  # Wing chord tip
                        'c_mean': data[36],  # Wing chord mean
                        'ws_max': data[37],  # Max wing loading
                        'ts_max': data[38],  # Max thrust loading
                        'g_max': data[39],  # G limit
                        'S': data[40],  # wing surface area
                        'sweep': data[41],  # Wing sweep
                        'sweep-remark': data[42],  # Remark about where sweep is measured
                        'n': data[43],  # Number of engines
                        'T': data[44],  # Total thrust

                        }
        elif data_dict is not None:
            assert len(data_dict) == 36  # This is to make sure that the exact same
            # dictionnary regardless of the way the information is entered
            # I know it doens't actually make sure, but it's something.
            self.dict = data_dict

    # This is some code to make it act like a dictionnary, but one that has functions
    # This is so we can easily add values such as C_L by calling a function onto this
    # class, but have all the benefits of a dict.
    
    def __setitem__(self, key, item):
        self.dict[key] = item


    def calc(self):
        print()

    def __getitem__(self, key):
        return self.dict[key]

    def __repr__(self):
        return repr(self.dict)

    def __len__(self):
        return len(self.dict)

    def __delitem__(self, key):
        del self.dict[key]

    def clear(self):
        return self.dict.clear()

    def copy(self):
        return self.dict.copy()

    def has_key(self, k):
        return k in self.dict

    def update(self, *args, **kwargs):
        return self.dict.update(*args, **kwargs)

    def keys(self):
        return self.dict.keys()

    def values(self):
        return self.dict.values()

    def items(self):
        return self.dict.items()

    def __contains__(self, item):
        return item in self.dict

    def __iter__(self):
        return iter(self.dict)

    def __str__(self):
        return f"<Aircraft {self.dict['name']}>"

    # def print_all(self):
    #     print("\n")
    #     print(self)
    #     values = self.__dict__['dict']
    #     for key in values:
    #         if values[key] is not None:
    #             if type(values[key]) is dict:
    #                 print(key)
    #                 dct = values[key]
    #                 for sub_key in dct:
    #                     print("          {}: {}".format(sub_key, dct[sub_key]))
    #             else:
    #                 print("{}: {}".format(key, self.__dict__['dict'][key]))
    #     print("\n")
    def print_all(self, data=None, level=0):
        if data is None:
            print("\n")
            print(self)
            data = self.__dict__['dict']
        for key in data:
            if type(data[key]) is dict:
                empty_str = '    ' * level
                print(empty_str+key+":")
                self.print_all(data=data[key], level=level+1)
            else:
                empty_str = '    ' * level
                if data[key] is not None:
                    print(f"{empty_str}{key}: {data[key]}")
