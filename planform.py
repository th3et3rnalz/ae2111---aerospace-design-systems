def planform():
    """

    :rtype: object
    """
    from math import sqrt, atan, tan, cos, pi
    from read_data import data

    airbas = data.all_aircraft[-1]
    sweep_25c = 23.3 * pi / 180  # Sweep at quarter chord length [rad]
    S = airbas['S']  # Surface Area [m^2]
    A = airbas['AR']  # Aspect Ratio
    gam = 5  # Dihedral angle [deg]

    b = sqrt(S * A)  # Wing span [m]

    lam = 0.2 * (2 - sweep_25c * pi / 180)  # Taper ratio
    c_r = (2 * S) / ((1 + lam) * b)  # Root chord [m]
    c_t = lam * c_r  # Tip chord [m]
    sweep_0c = atan(tan(sweep_25c) + (c_r/(2*b) * (1-lam)))
    mac = 2 / 3 * c_r * (1 + lam + lam ** 2) / (1 + lam)  # Geometric chord length [m]
    sweep_50c = tan(sweep_0c) - c_r/b * (1-lam)
    y_lemac = atan(b / 6 * (1 + 2 * lam) / (1 + lam))  # Y position of the geometric chord [m]
    x_lemac = y_lemac * tan(sweep_0c)  # X position of the geometric chord [m]
    e_oswald = 4.61*(1-0.045 * airbas['AR']**0.68)*(cos(sweep_0c)**0.15)-3.1

    planform_results = {'lambda': lam,
                        'c_r': c_r,
                        'c_t': c_t,
                        'mac': mac,
                        'sweep_0.5c': sweep_50c,
                        'sweep_0c': sweep_0c,
                        'sweep_0.25c': sweep_25c,
                        'x_lemac': x_lemac,
                        'y_lemac': y_lemac,
                        'e': e_oswald,
                        'b': b,
                        'dihedral': gam
                        }

    return planform_results
