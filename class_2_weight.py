
def class_ii_weight(w_mto, tail=None, cg_excursion=None, m_fuel=None):
    from read_data import data
    from math import cos, pi, sqrt

    airbas = data.all_aircraft[-1]
    if m_fuel is None:
        w_fuel = airbas['m_fuel'] * 2.20462  # [lb]
    else:
        w_fuel = m_fuel * 2.20462  # [lb]
    w_zf = w_mto - w_fuel  # [lb]
    sweep_50c = airbas['planform']['sweep_0.5c']  # [rad]
    b = airbas['planform']['b']*3.28  # [ft]
    S = airbas['S']*10.7639  # [ft^2]
    r_ferry = airbas['r_ferry']

    # Making sure that the relationship for n_max from is valid
    assert 4100 < w_mto < 50000
    n_max = 2.1 + 24000 / (w_mto + 10000)
    n_ult = 1.5*n_max

    # Some more variables
    tr = airbas['planform']['c_r']*3.28*0.21
    v_d = (airbas['v_cruise'] * 1.25) / 0.514444  # According to Roskam: v_d >= v_c * 1.25, so we take v_d = v_c *1.25
    t_to = airbas['T']

    """
    The formulae used in this section are from Roskam Part V: Component weight estimation
    Weight estimation overview (ISBN 1-884885-50-0)
    """

    # ==============================================================
    # STRUCTURE WEIGHT  -  STRUCTURE WEIGHT  -  STRUCTURE WEIGHT
    # ==============================================================

    # Wing weight estimation, page 69 (Torenbeek)

    w_wing = 0.0017 * w_zf * (b/cos(sweep_50c))**0.75 * (1 + (6.3*cos(sweep_50c)/b)**0.5) * n_ult**0.55 * \
        (b*S / (tr * cos(sweep_50c) * w_zf))

    # Empennage weight estimation, page 74 (Torenbeek)
    if tail is not None:
        S_h = tail['h_tail']['S_h'] * 10.7639  # Horizontal tail area [ft^2]
    else:
        S_h = 12 * 10.7639  # Horizontal tail area [ft^2]
    K_h = 1.1  # Assuming a trim-able tail
    if tail is not None:
        sweep_50c_h_tail = tail['h_tail']['sweep_50c_h']  # The horizontal tail half chord
    else:
        sweep_50c_h_tail = 0.439  # The horizontal tail half chord

    w_h_tail = K_h*S_h*(3.81*(S_h**0.2*v_d) / (1000*cos(sweep_50c_h_tail)**0.5) - 0.287)

    if tail is not None:
        S_v = tail['v_tail']['S_v'] * 10.7639  # Vertical tail area [ft^2]
        z_h = tail['v_tail']['b_v'] / 0.3048  # Distance from the vertical tail root to where the horizontal tail
        # is mounted on the vertical tail, in ft.
        b_v = tail['v_tail']['b_v'] / 0.3048  # vertical tail span in ft
        K_v = 1 + 0.15 * (S_h*z_h / (S_v * b_v))
        sweep_50c_v_tail = tail['v_tail']['sweep_50c_v']  # The vertical tail half chord sweep angle
    else:
        S_v = 12 * 10.7639  # Vertical tail area [ft^2]
        z_h = 3 / 0.3048  # Distance from the vertical tail root to where the horizontal tail
        # is mounted on the vertical tail, in ft.
        b_v = 3 / 0.3048  # vertical tail span in ft
        K_v = 1 + 0.15 * (S_h * z_h / (S_v * b_v))
        sweep_50c_v_tail = 0.442  # The vertical tail half chord sweep angle

    w_v_tail = K_v*S_v * (3.81 * (S_v**0.2 * v_d / (1000*cos(sweep_50c_v_tail)**0.5)) - 0.287)  #

    w_empennage = w_v_tail + w_h_tail
    # Fuselage weight estimation

    D = 2  # fuselage diameter [m]
    L_1 = 3  # nosecone length [m]
    L_3 = 8  # tailcone length [m]
    L_2 = 19 - L_1 - L_3  # Cabin length [m]
    S_fg = pi*D/4 * (1/(3*L_1**2) * ((4*L_1**2 + D**2/4)**1.5 - D**3/8) - D + 4*L_2 + 2*sqrt(L_3**2 + D**2/4))
    # fuselage surface area [m^2]

    K_f = 1.08  # We'll be having a pressurized fuselage
    if cg_excursion is not None:
        l_h = (19 - cg_excursion['X_lemac'] - (19 - (0.9*19 + 0.5*(tail['v_tail']['x_lemac_v'] +
                                                             tail['h_tail']['x_lemac_h']))))*3.281
    else:
        l_h = 7 / 0.3048
    # distance from wing root c/4 to horizontal tail root c/4 [ft]
    w_f = 2 * 3.281  # fuselage width [ft]
    h_f = 2 * 3.281  # fuselage height, we assume a circular fuselage [ft]

    assert v_d > 250 * 0.51444
    w_fuselage = 0.021*K_f*(v_d * l_h / (w_f * h_f))**0.5 * (10.76391*S_fg)**1.2

    # Nacelle weight estimation
    w_n = 0.065 * t_to * 0.224809

    # Landing gear weight estimation
    K_g_r = 1.0
    w_lg_nose = K_g_r * (12 + 0.06*w_mto**0.75)
    w_lg_main = K_g_r * (33 + 0.04*w_mto**0.75 + 0.021*w_mto)

    # RESULTANT EMPTY WEIGHT
    w_e = w_wing + w_h_tail + w_v_tail + w_fuselage + w_n + w_lg_main + w_lg_nose

    # ==============================================================
    #  POWERPLANT  -   POWERPLANT  -   POWERPLANT  -   POWERPLANT
    # ==============================================================

    # engine weight
    w_engines = 2 * 450 * 2.20462

    # Fuel system weight estimation
    N_e = 2
    N_t = 2
    w_fs = 80 * (N_e + N_t - 1) + 15*N_t**0.5 * (w_fuel/5.87)**0.333
    # w_fs = 200

    w_pwrpl = w_engines + w_fs
    # ==============================================================
    #      FIXED EQUIPMENT WEIGHT  -   FIXED EQUIPMENT WEIGHT
    # ==============================================================

    # Flight control system weight estimation
    K_fc = 0.64
    w_fc = K_fc*w_mto**(2/3)

    # Hydraulic and/or pneumatic weight estimation
    w_hydraulic = 0.00110 * w_mto

    # Electrical system weight estimation
    v_pax = 20 * 35.3147  # Passenger cabin volume [ft^3]
    w_els = 10.8 * v_pax**0.7 * (1 - 0.018 * v_pax ** 0.35)

    # Weight estimation for instrumentation, avionics and electronics
    w_iae = 0.575 * w_e**0.556 * (r_ferry*0.5399565)**0.25

    # Weight estimation for pressurization, AC, anti- and de-icing systems
    l_pax = 11.46 * 3.281  # cabin length [ft]
    w_api = 6.75 * l_pax ** 1.28

    # Weight estimation for the oxygen system
    n_pax = 8  # We take 8 passengers
    w_ox = 40 + 2.4 * n_pax

    # Auxiliary power unit weight estimation
    w_apu = 0.0085 * w_mto

    # Furnishings weight estimation

    w_fur = 0.211*(w_mto - w_fuel)**0.91

    # Weight estimation of auxilliary gear
    w_aux = 0.01 * w_e

    # Paint weight estimation
    w_paint = 0.0045 * w_mto

    w_equipment = w_fc + w_hydraulic + w_els + w_iae + w_api + w_ox + w_apu + w_fur + w_aux + w_paint

    m_total = (w_equipment + w_e + w_fuel + w_engines) / 2.20462 + airbas['m_payload']
    m_oe = m_total - airbas['m_payload'] - airbas['m_fuel']

    class_ii_mass = {'m_equipment': w_equipment/2.20462, 'm_powerplant': w_pwrpl/2.20462, 'w_empty': w_e/2.20462,
                     'm_wing': w_wing/2.20462, 'm_empennage': w_empennage/2.20562, 'm_nacelle': w_n/2.20462,
                     'm_fuselage': w_fuselage/2.20462, 'm_mto': m_total, 'm_oe': m_oe}
    return class_ii_mass
