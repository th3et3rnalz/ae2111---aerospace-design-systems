def empennage(cg_result):
    from math import tan, atan, sqrt, radians
    from read_data import data
    airbas = data.all_aircraft[-1]

    Vh = 0.720625                                               #Volume fraction statistical relations
    Vv = 0.072875

    X_aftcg = cg_result['cg_max']                               #Most aft cg
    LengthFuselage = 19
    X_EMP = 0.9*LengthFuselage                                  #Empennage location
    Xv_Xaftcg = X_EMP - X_aftcg                                 #Moment arm vertical tail
    Xh_Xaftcg = Xv_Xaftcg                                       #Moment arm NON T-TAIL CONFIG
    S = airbas['S']                                             #Surface Area
    b = airbas['planform']['b']                                 #span
    MAC = airbas['planform']['mac']                             #MAC WING
    Sh = Vh*S*MAC/Xh_Xaftcg                                     #Surface horizontal tail
    Sv = Vv*S*b/Xv_Xaftcg                                       #Surface vertical tail

    Ah = 4                                                      #Aspect ratio horizontal tail
    Av = 1.1                                                    #Aspect ratio vertical tail
    Taper_h = 0.4                                               #Taper ratio horizontal tail
    Taper_v = 0.6                                               #Taper ratio vertical tail
    QCS_H = 30                                                  #Quarter chord sweep horizontal tail
    QCS_V = 35                                                  #Quarter chord sweep vertical tail

    b_h = sqrt(Sh*Ah)                                           #span horizontal tail
    Crh = 2*Sh/((1+Taper_h)*b_h)                                #root chord horizontal tail
    Cth = Taper_h * Crh                                         #Tip chord horizontal tail
    MACh = 2/3*Crh*(1+Taper_h+ Taper_h**2)/(1+Taper_h)          #MAC horizontal tail
    LES_h = atan(tan(radians(QCS_H))+Crh*(1-Taper_h)/2/b_h)     #Leading edge sweep horizontal tail
    HALFS_h = atan(tan(LES_h)-Crh*(1-Taper_h)/b_h)              #Half chord sweep horizontal tail
    Y_MACh = b_h/6*(1+2*Taper_h)/(1+Taper_h)                    #Y coord MAC
    X_MACh = tan(LES_h)*Y_MACh

    b_v = sqrt(Sv*Av)                                           #span vertical tail
    Crv = 2*Sv/((1+Taper_v)*b_v)                                #root chord vertical tail
    Ctv = Taper_v * Crv                                         #Tip chord vertical tail
    MACv = 2/3*Crv*(1+Taper_v+ Taper_v**2)/(1+Taper_v)          #MAC vertical tail
    LES_v = atan(tan(radians(QCS_V))+Crv*(1-Taper_v)/2/b_v)     #Leading edge sweep vertical tail
    HALFS_v = atan(tan(LES_v)-Crv*(1-Taper_v)/b_v)              #Half chord sweep vertical tail
    Y_MACv = b_v/6*(1+2*Taper_v)/(1+Taper_v)                    #Y coord MAC vertical tail
    X_MACv = tan(LES_v)*Y_MACv                                  #X coord MAC vertical tail


    #CALCULATE HORIZONTAL STABILIZER AGAIN BECAUSE OF T_TAIL CONFIGURATION
    ShiftHorizontal = tan(radians(QCS_V))*b_v
    Xh_Xaftcg = Xv_Xaftcg + ShiftHorizontal
    Sh = Vh*S*MAC/Xh_Xaftcg                                     #Surface horizontal tail

    b_h = sqrt(Sh*Ah)                                           #span horizontal tail
    Crh = 2*Sh/((1+Taper_h)*b_h)                                #root chord horizontal tail
    Cth = Taper_h * Crh                                         #Tip chord horizontal tail
    MACh = 2/3*Crh*(1+Taper_h + Taper_h**2)/(1+Taper_h)          #MAC horizontal tail
    LES_h = atan(tan(radians(QCS_H))+Crh*(1-Taper_h)/2/b_h)     #Leading edge sweep horizontal tail
    HALFS_h = atan(tan(LES_h)-Crh*(1-Taper_h)/b_h)              #Half chord sweep horizontal tail
    Y_MACh = b_h/6*(1+2*Taper_h)/(1+Taper_h)                    #Y coord MAC vertical tail
    X_MACh = tan(LES_h)*Y_MACh                                  #X coord MAC vertical tail

    h_tail = {'mac_h': MACh, 'b_h': b_h, 'c_r_h': Crh, 'c_t_h': Cth, 'sweep_0c_h': LES_h, 'sweep_50c_h': HALFS_h,
              'y_lemac_h': Y_MACh, 'x_lemac_h': X_MACh, 'S_h': Sh}

    v_tail = {'mac_v': MACv, 'b_v': b_v, 'c_r_v': Crv, 'c_t_v': Ctv, 'sweep_0c_v': LES_v, 'sweep_50c_v': HALFS_v,
              'y_lemac_v': Y_MACv, 'x_lemac_v': X_MACv, 'S_v': Sv}

    emp = {'h_tail': h_tail, 'v_tail': v_tail}
    return emp
