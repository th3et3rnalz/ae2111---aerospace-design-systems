import csv, math
from isa import ISA
from aircraft import Aircraft
from our_aircraft import our_business_jet
import matplotlib.pyplot as plt
import numpy

class Data:
    def __init__(self, debug=False):
        self.all_aircraft = []
        self.debug = debug
    
    def add_aircraft(self, aircraft):
        self.all_aircraft.append(aircraft)

    def clean(self):
        for aircraft in self.all_aircraft:
            if aircraft['name'] == None:
                index = self.all_aircraft.index(aircraft)
                if self.debug:
                    print("REMOVE:",self.all_aircraft[index])
                self.all_aircraft.pop(index)

    def get_list(self, list_name):
        lists = self.all_aircraft[0].keys()

        list_to_return = []
        if list_name in lists:
            for aircraft in self.all_aircraft:
                list_to_return.append(aircraft[list_name])
        else:
            raise Exception('No such attribute')
        
        return list_to_return
    
    def add_entry(self, entry, default=None):
        for aircraft in self.all_aircraft:
            aircraft[entr] = default

    def graph(self, x_list, y_list, title=None, xlabel=None, ylabel=None, fit=False, plot=False, x_scale=1, y_scale=1):
        if plot:           
            # plt.show()
            pass
        x_lst = self.get_list(x_list)
        y_lst = self.get_list(y_list)

        x, y = [], []
        for el in x_lst:
            if el and y_lst[x_lst.index(el)] is not None:
                x.append(el*x_scale)
                y.append(y_lst[x_lst.index(el)]*y_scale)

        plt.scatter(x, y)
        if type(title) is str:
            plt.title(title)
        if type(xlabel) is str:
            plt.xlabel(xlabel)
        if type(ylabel) is str:
            plt.ylabel(ylabel)
    
        if fit:
            z = numpy.polyfit(x, y, fit)
            p = numpy.poly1d(z)
            
        if plot:
            if fit:
                x = numpy.linspace(30, 60)
                plt.plot(x, p(x))
            plt.show()

        if fit:
            return p, z

    def avg(self,lst):
        values = self.get_list(lst)
        while None in values:
            for val in values:
                if val is None:
                    values.pop(values.index(val))
                    break
        avg = sum(values)/len(values)

        return avg


def clean_input(value):
    if type(value) is str:
        try:
            return float(value.replace(',', '.'))
        except:
            if value == '':
                return None
            else:
                return value



# READING FROM CSV FILE
with open('AirplaneComparison3.xlsx - Blad1.csv') as f:
    aircraft = list(csv.reader(f))
    names_list = aircraft[1][2:]

data = Data()
for num in range(len(names_list)):
    plane = [clean_input(row[num]) for row in aircraft]
    if plane[1] is not '':
        data.add_aircraft(Aircraft(plane))

# Let's add the aircraft we will design
data.add_aircraft(our_business_jet)

# REMOVE LEFT HAND COLUMN
data.all_aircraft.pop(0)
data.clean()



if __name__ == "__main__":  # i.e. only run this when this is the file ran, not when imported
    print("Aircraft ingested:", data.get_list('name'))
    # Let's have a look at our plane:
    print("Our aircraft:", data.all_aircraft[-1])

