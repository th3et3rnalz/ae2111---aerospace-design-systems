from math import sqrt, pi
import matplotlib.pyplot as plt
import numpy as np
from read_data import data
from isa import ISA


"""
REQUIRED INPUTS:

From ISA: density & density0
From Aircraft: v_cruise
From slides: C_L_max, dc_0, A, e, C_L_take_off, TOP
From requirements: v_stall
From class1: WL/Wto

We define x and y as follows:

    x = w/s (wing loading)
    y = t/s (thrust loading)

"""


def stall(density, stallspeed, clmax):
    xstall = []
    ystall = [0,1]
    x = 0.5*density*stallspeed**2*clmax
    for i in range(2):
        xstall.append(x)

    return xstall, ystall


def takeoff(density0, density, cltakeoff, takeoffperformance):
    ytakeoff = []
    xtakeoff = []
    cltakeoff = cltakeoff/1.21
    for x in range(100,5000,10):
        y = x/(takeoffperformance*cltakeoff*(density/density0)**1.75)
        ytakeoff.append(y)
        xtakeoff.append(x)

    return xtakeoff, ytakeoff


def land(density, clmax, wlwto, vstall):
    # print(wlwto)
    constant = 0.5847
    x = clmax*density/(2*wlwto) * 1.21 * vstall**2
    xland = []
    yland = [0, 1]

    for i in range(2):
        xland.append(x)

    return xland, yland


def cruise(density0,density,cruisespeed,cdo,aspectratio,oswald,loadfactor):
    ts  = 0.9
    mto = 0.8
    xcruise = []
    ycruise = []

    for x in range(250, 5000, 10):
        y = (mto/ts)*(density0/density)**(3/4)*((cdo*0.5*density*cruisespeed**2)/(mto*x) + (mto*x*loadfactor**2)/(pi*aspectratio*oswald*0.5*density*cruisespeed**2))
        ycruise.append(y)
        xcruise.append(x)

    return xcruise, ycruise


def climbrate(density,cdo, climbratems, aspect,oswald):
    cl = sqrt(3*cdo*pi*aspect*oswald)
    cd = 4*cdo
    yclimbrate = []
    xclimbrate = []

    for x in range(100, 5000, 10):
        y = (climbratems)/(sqrt(2*x/(density*cl))) + cd/cl
        yclimbrate.append(y)
        xclimbrate.append(x)

    return xclimbrate, yclimbrate


def climbangle(cdo, aspect, oswald, oei):
    coverv = 0.032
    yclimbangle = []
    xclimbangle = [0, 5000]
    cdo = cdo*1.05
    n = 2  # number engines

    for i in range(2):
        y = (n/(n-oei))*((coverv) + 2 * sqrt(cdo / (pi * aspect * oswald)))
        yclimbangle.append(y)

    return xclimbangle, yclimbangle


# VALUES  VALUES  VALUES  VALUES  VALUES  VALUES  VALUES  VALUES  VALUES  VALUES 
# VALUES  VALUES  VALUES  VALUES  VALUES  VALUES  VALUES  VALUES  VALUES  VALUES 
isa = ISA()
rho = isa.get(4100*0.3048)
airbas = data.all_aircraft[-1]  # Airbas is last in list
v_cruise = airbas['v_cruise']  # [m/s]
v_stall = airbas['v_stall']  # [m/s]

rho0 = 1.225
# clean , takeoff ,landing
clmax = [1.8, 2.2, 2.37]
ar = airbas['AR']
e = [0.85, 0.9, 0.95]
# clean, takeoff gearup , takeoff gear down, landing config, landing gear up
cd0 = [0.0156, 0.0270, 0.0420, 0.0750, 0.0600]  # from slides but are pretty random
cd = 0.05
ml_mto = (airbas['m_oe'] + airbas['m_payload']) / (airbas['m_mto'])

# END-VALUES  END-VALUES  END-VALUES  END-VALUES  END-VALUES  END-VALUES  END-VALUES
# END-VALUES  END-VALUES  END-VALUES  END-VALUES  END-VALUES  END-VALUES  END-VALUES



#(density, stallspeed, CLmax)

# x1,y1 = stall(1.111652282, v_stall, clmax[2])
#(density0, density, CLtakeoff, TOP)
x2,y2 = takeoff(rho0,1.112,clmax[1],4637)
#( density, CLmax, (WL/Wto))
x3,y3 = land(1.111652282, clmax[2], ml_mto, v_stall)
#(density0, density, cruisespeed, cd0,aspectratio,oswald,loadfactor)
x4,y4 = cruise(rho0,0.299278665, 196, cd0[0],ar,e[0],1.5)
#(density, cd0, climbrate, aspect,oswald)
x5,y5 = climbrate(rho0,cd0[0],10,ar,e[1])
#(cd0, aspectratio, oswald,oei)
x6,y6 = climbangle(cd0[4], ar, e[2],1)

z1 = np.array(y5)
z2 = np.full((390),0.268555)
xx = np.linspace((100),(5000),390)


# if __name__ == "__main__":
plt.rcParams.update({'mathtext.default':  'regular' })
# plt.plot(x1,y1,label = r'$ v_{stall} $')#blue --> stall  WE DISREGARD STALL
plt.plot(x2, y2, label='Take-off')  # orange --> take off
plt.plot(x3, y3, label='Land')  # green --> land
plt.plot(x4, y4, label=r'$ v_{cruise} $')  # red --> cruise
plt.plot(x5, y5, label='RoC')  # purple --> climbrate in m/s
plt.plot(x6, y6, label=r'$ \alpha_{climb} $')  # brown --> climangle
plt.plot(2410, 0.355, 'ro', label='Design point')

for aircraft in data.all_aircraft:
    mass = aircraft['m_mto']
    thrust = aircraft['T']
    S = aircraft['S']
    if mass and thrust and S is not None:
        w_s = mass * 9.80665 / S
        t_w = thrust / (mass * 9.08665)
        if aircraft['name'] == 'Pilatus PC-24':
            plt.plot(w_s, t_w, 'go', label='Pilatus PC-24')
        else:
            plt.plot(w_s, t_w, 'bo')

plt.legend(bbox_to_anchor=(0.69, 1), loc='upper left', borderaxespad=0.)
plt.grid(b=None, which='major', axis='both')
plt.xlabel('W/S')
plt.ylabel('T/W')
plt.title('T/W - W/S')
plt.xlim([0, 5000])
plt.ylim([0, 1])
plt.show()
plt.cla()
plt.clf()

# OLD DESIGN POINT, DO NOT REMOVE
design_point = {'W/S': 2410, 'T/W': 0.355}